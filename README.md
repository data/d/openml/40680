# OpenML dataset: mofn-3-7-10

https://www.openml.org/d/40680

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Unknown  
**Source**: [PMLB](https://github.com/EpistasisLab/penn-ml-benchmarks/tree/master/datasets/classification) Supposedly from UCI originally, but can't find it there.  
**Please cite**  

The origin is not clear, but presumably this is an artificial problem representing M-of-N rules. The target is 1 if a certain M 'bits' are '1'? (Joaquin Vanschoren)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40680) of an [OpenML dataset](https://www.openml.org/d/40680). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40680/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40680/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40680/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

